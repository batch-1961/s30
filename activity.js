db.fruits.aggregate([
    {$match: {$and:[{price:{$lt:50},supplier:"Yellow Farms"}]}},
    {$count:"ItemLower50"}
    
 ])


db.fruits.aggregate([
    {$match: {price:{$lt:30}}},
    {$count:"priceLessThan30"}
 ])


db.fruits.aggregate([
    {$match: {supplier:"Yellow Farms"}},
    {$group: {_id:"$supplier",avgPrice:{$avg:"$price"}}}
 ])

db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"$supplier",highestPrice:{$max:"$price"}}}
 ])

db.fruits.aggregate([
    {$match: {supplier:"Red Farms Inc."}},
    {$group: {_id:"$supplier",lowestPrice:{$min:"$price"}}}
 ])